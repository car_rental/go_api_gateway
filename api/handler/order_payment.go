package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/car_rental/go_api_gateway/genproto/order_service"
	"gitlab.com/car_rental/go_api_gateway/genproto/user_service"
	"gitlab.com/car_rental/go_api_gateway/pkg/helper"
)

// Create order_payment godoc
// @ID create_order_payment
// @Router /order_payment [POST]
// @Summary Create OrderPayment
// @Description Create OrderPayment
// @Tags OrderPayment
// @Accept json
// @Procedure json
// @Param OrderPayment body order_service.OrderPaymentCreate true "CreateOrderPaymentRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) CreateOrderPayment(ctx *gin.Context) {
	var OrderPayment order_service.OrderPaymentCreate

	err := ctx.ShouldBind(&OrderPayment)
	if err != nil {
		h.handlerResponse(ctx, "CreateOrderPayment", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.OrderPaymentService().Create(ctx, &OrderPayment)
	if err != nil {
		h.handlerResponse(ctx, "OrderPaymentService().Create", http.StatusBadRequest, err.Error())
		return
	}

	// Get Order To Get Client id
	order, err := h.services.OrderService().GetById(ctx, &order_service.OrderPrimaryKey{
		Id: OrderPayment.OrderId,
	})
	if err != nil {
		h.handlerResponse(ctx, "OrderPaymentService().Create=>h.services.OrderService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	// Get Client Email
	client_email, err := h.services.ClientService().GetById(ctx, &user_service.ClientPrimaryKey{
		Id: order.ClientId,
	})
	if err != nil {
		h.handlerResponse(ctx, "OrderPaymentService().Create=>ClientService().GetById", http.StatusBadRequest, err.Error())
		return
	}
	if order.Paid == "Fully" {
		err = h.SendMessageToMail("rentcartest123@gmail.com", "qngugcmjckgpuago", client_email.Email, "Congratulations you paid the service Of Car Rental")
		if err != nil {
			h.handlerResponse(ctx, "CreateOrderPayment=>SendMessageToMail", http.StatusBadRequest, err.Error())
			return
		}
	} else {
		err = h.SendMessageToMail("rentcartest123@gmail.com", "qngugcmjckgpuago", client_email.Email, "You not fully paid you have debt the service Of Car Rental")
		if err != nil {
			h.handlerResponse(ctx, "CreateOrderPayment=>SendMessageToMail", http.StatusBadRequest, err.Error())
			return
		}
	}
	h.handlerResponse(ctx, "create OrderPayment resposne", http.StatusCreated, resp)
}

// GetByID order_payment godoc
// @ID get_by_id_order_payment
// @Router /order_payment/{id} [GET]
// @Summary Get By ID OrderPayment
// @Description Get By ID OrderPayment
// @Tags OrderPayment
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) GetByIdOrderPayment(ctx *gin.Context) {
	OrderPaymentId := ctx.Param("id")

	if !helper.IsValidUUID(OrderPaymentId) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "Invaid uuid")
		return
	}

	resp, err := h.services.OrderPaymentService().GetById(ctx, &order_service.OrderPaymentPrimaryKey{Id: OrderPaymentId})
	if err != nil {
		h.handlerResponse(ctx, "OrderPaymentService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get by id OrderPayment resposne", http.StatusOK, resp)
}

// GetList order_payment godoc
// @ID get_list_order_payment
// @Router /order_payment [GET]
// @Summary Get List OrderPayment
// @Description Get List OrderPayment
// @Tags OrderPayment
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) OrderPaymentGetList(ctx *gin.Context) {
	offset, err := h.getOffsetQuery(ctx.Query("offset"))
	if err != nil {
		h.handlerResponse(ctx, "get list OrderPayment offset", http.StatusBadRequest, "invalid offset")
		return
	}

	limit, err := h.getLimitQuery(ctx.Query("limit"))
	if err != nil {
		h.handlerResponse(ctx, "get list OrderPayment limit", http.StatusBadRequest, "invalid limit")
		return
	}

	resp, err := h.services.OrderPaymentService().GetList(ctx.Request.Context(), &order_service.OrderPaymentGetListRequest{
		Offset: int64(offset),
		Limit:  int64(limit),
		Search: ctx.Query("search"),
	})
	if err != nil {
		h.handlerResponse(ctx, "OrderPaymentService().GetList", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "get list OrderPayment resposne", http.StatusOK, resp)
}

// Update order_payment godoc
// @ID update_order_payment
// @Router /order_payment/{id} [PUT]
// @Summary Update OrderPayment
// @Description Update OrderPayment
// @Tags OrderPayment
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param OrderPayment body order_service.OrderPaymentUpdate true "UpdateOrderPaymentRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) OrderPaymentUpdate(ctx *gin.Context) {
	var (
		id                 string = ctx.Param("id")
		OrderPaymentupdate order_service.OrderPaymentUpdate
	)

	if !helper.IsValidUUID(id) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	err := ctx.ShouldBindJSON(&OrderPaymentupdate)
	if err != nil {
		h.handlerResponse(ctx, "error OrderPayment should bind json", http.StatusBadRequest, err.Error())
		return
	}

	OrderPaymentupdate.Id = id
	resp, err := h.services.OrderPaymentService().Update(ctx.Request.Context(), &OrderPaymentupdate)
	if err != nil {
		h.handlerResponse(ctx, "services.OrderPaymentService().Update", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "create OrderPayment resposne", http.StatusAccepted, resp)
}

// Delete order_payment godoc
// @ID delete_order_payment
// @Router /order_payment/{id} [DELETE]
// @Summary Delete OrderPayment
// @Description Delete OrderPayment
// @Tags OrderPayment
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) DeleteOrderPayment(c *gin.Context) {

	var id string = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.handlerResponse(c, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	resp, err := h.services.OrderPaymentService().Delete(c.Request.Context(), &order_service.OrderPaymentPrimaryKey{Id: id})
	if err != nil {
		h.handlerResponse(c, "services.OrderPaymentService().Delete", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "create OrderPayment resposne", http.StatusNoContent, resp)
}
