package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/car_rental/go_api_gateway/genproto/user_service"
	"gitlab.com/car_rental/go_api_gateway/pkg/helper"
)

// Create investor godoc
// @ID create_investor
// @Router /investor [POST]
// @Summary Create Investor
// @Description Create Investor
// @Tags Investor
// @Accept json
// @Procedure json
// @Param Investor body user_service.InvestorCreate true "CreateInvestorRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) CreateInvestor(ctx *gin.Context) {
	var investor user_service.InvestorCreate

	err := ctx.ShouldBind(&investor)
	if err != nil {
		h.handlerResponse(ctx, "CreateInvestor", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.InvestorService().Create(ctx, &investor)
	if err != nil {
		h.handlerResponse(ctx, "InvestorService().Create", http.StatusBadRequest, err.Error())

		return
	}

	h.handlerResponse(ctx, "create investor resposne", http.StatusCreated, resp)
}

// GetByID investor godoc
// @ID get_by_id_investor
// @Router /investor/{id} [GET]
// @Summary Get By ID Investor
// @Description Get By ID Investor
// @Tags Investor
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) GetByIdInvestor(ctx *gin.Context) {
	investorId := ctx.Param("id")

	if !helper.IsValidUUID(investorId) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "Invaid uuid")
		return
	}

	resp, err := h.services.InvestorService().GetById(ctx, &user_service.InvestorPrimaryKey{Id: investorId})
	if err != nil {
		h.handlerResponse(ctx, "InvestorService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get by id investor resposne", http.StatusOK, resp)
}

// GetList investor godoc
// @ID get_list_investor
// @Router /investor [GET]
// @Summary Get List Investor
// @Description Get List Investor
// @Tags Investor
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param Search query string false "search"
// @Param Search-By-Phone-Number query string false "search-by-phone-number"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) InvestorGetList(ctx *gin.Context) {
	offset, err := h.getOffsetQuery(ctx.Query("offset"))
	if err != nil {
		h.handlerResponse(ctx, "get list investor offset", http.StatusBadRequest, "invalid offset")
		return
	}

	limit, err := h.getLimitQuery(ctx.Query("limit"))
	if err != nil {
		h.handlerResponse(ctx, "get list investor limit", http.StatusBadRequest, "invalid limit")
		return
	}

	resp, err := h.services.InvestorService().GetList(ctx.Request.Context(), &user_service.InvestorGetListRequest{
		Offset:              int64(offset),
		Limit:               int64(limit),
		Search:              ctx.Query("search"),
		SearchByPhoneNumber: ctx.Query("search-by-phone-number"),
	})
	if err != nil {
		h.handlerResponse(ctx, "InvestorService().GetList", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "get list investor resposne", http.StatusOK, resp)
}

// Update investor godoc
// @ID update_investor
// @Router /investor/{id} [PUT]
// @Summary Update Investor
// @Description Update Investor
// @Tags Investor
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param investor body user_service.InvestorUpdate true "UpdateInvestorRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) InvestorUpdate(ctx *gin.Context) {
	var (
		id             string = ctx.Param("id")
		Investorupdate user_service.InvestorUpdate
	)

	if !helper.IsValidUUID(id) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	err := ctx.ShouldBindJSON(&Investorupdate)
	if err != nil {
		h.handlerResponse(ctx, "error investor should bind json", http.StatusBadRequest, err.Error())
		return
	}

	Investorupdate.Id = id
	resp, err := h.services.InvestorService().Update(ctx.Request.Context(), &Investorupdate)
	if err != nil {
		h.handlerResponse(ctx, "services.InvestorService().Update", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "create investor resposne", http.StatusAccepted, resp)
}

// Delete investor godoc
// @ID delete_investor
// @Router /investor/{id} [DELETE]
// @Summary Delete Investor
// @Description Delete Investor
// @Tags Investor
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) DeleteInvestor(c *gin.Context) {

	var id string = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.handlerResponse(c, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	resp, err := h.services.InvestorService().Delete(c.Request.Context(), &user_service.InvestorPrimaryKey{Id: id})
	if err != nil {
		h.handlerResponse(c, "services.InvestorService().Delete", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "create investor resposne", http.StatusNoContent, resp)
}
