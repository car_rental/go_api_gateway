package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/car_rental/go_api_gateway/genproto/user_service"
	"gitlab.com/car_rental/go_api_gateway/pkg/helper"
)

// Create branch godoc
// @ID create_branch
// @Router /branch [POST]
// @Summary Create Branch
// @Description Create Branch
// @Tags Branch
// @Accept json
// @Procedure json
// @Param Branch body user_service.BranchCreate true "CreateBranchRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) CreateBranch(ctx *gin.Context) {
	var Branch user_service.BranchCreate

	err := ctx.ShouldBind(&Branch)
	if err != nil {
		h.handlerResponse(ctx, "CreateBranch", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.BranchService().Create(ctx, &Branch)
	if err != nil {
		h.handlerResponse(ctx, "BranchService().Create", http.StatusBadRequest, err.Error())

		return
	}

	h.handlerResponse(ctx, "create Branch resposne", http.StatusOK, resp)
}

// GetByID branch godoc
// @ID get_by_id_branch
// @Router /branch/{id} [GET]
// @Summary Get By ID Branch
// @Description Get By ID Branch
// @Tags Branch
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) GetByIdBranch(ctx *gin.Context) {
	BranchId := ctx.Param("id")

	if !helper.IsValidUUID(BranchId) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "Invaid uuid")
		return
	}

	resp, err := h.services.BranchService().GetById(ctx, &user_service.BranchPrimaryKey{Id: BranchId})
	if err != nil {
		h.handlerResponse(ctx, "BranchService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get by id Branch resposne", http.StatusCreated, resp)
}

// GetList branch godoc
// @ID get_list_branch
// @Router /branch [GET]
// @Summary Get List Branch
// @Description Get List Branch
// @Tags Branch
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param search query string false "search"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) BranchGetList(ctx *gin.Context) {
	offset, err := h.getOffsetQuery(ctx.Query("offset"))
	if err != nil {
		h.handlerResponse(ctx, "get list Branch offset", http.StatusBadRequest, "invalid offset")
		return
	}

	limit, err := h.getLimitQuery(ctx.Query("limit"))
	if err != nil {
		h.handlerResponse(ctx, "get list Branch limit", http.StatusBadRequest, "invalid limit")
		return
	}

	resp, err := h.services.BranchService().GetList(ctx.Request.Context(), &user_service.BranchGetListRequest{
		Offset: int64(offset),
		Limit:  int64(limit),
		Search: ctx.Query("search"),
	})
	if err != nil {
		h.handlerResponse(ctx, "BranchService().GetList", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "get list Branch resposne", http.StatusOK, resp)
}

// Update branch godoc
// @ID update_branch
// @Router /branch/{id} [PUT]
// @Summary Update Branch
// @Description Update Branch
// @Tags Branch
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param Branch body user_service.BranchUpdate true "UpdateBranchRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) BranchUpdate(ctx *gin.Context) {
	var (
		id           string = ctx.Param("id")
		Branchupdate user_service.BranchUpdate
	)

	if !helper.IsValidUUID(id) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	err := ctx.ShouldBindJSON(&Branchupdate)
	if err != nil {
		h.handlerResponse(ctx, "error Branch should bind json", http.StatusBadRequest, err.Error())
		return
	}

	Branchupdate.Id = id
	resp, err := h.services.BranchService().Update(ctx.Request.Context(), &Branchupdate)
	if err != nil {
		h.handlerResponse(ctx, "services.BranchService().Update", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "create Branch resposne", http.StatusAccepted, resp)
}

// Delete branch godoc
// @ID delete_branch
// @Router /branch/{id} [DELETE]
// @Summary Delete Branch
// @Description Delete Branch
// @Tags Branch
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) DeleteBranch(c *gin.Context) {

	var id string = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.handlerResponse(c, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	resp, err := h.services.BranchService().Delete(c.Request.Context(), &user_service.BranchPrimaryKey{Id: id})
	if err != nil {
		h.handlerResponse(c, "services.BranchService().Delete", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "create Branch resposne", http.StatusNoContent, resp)
}
