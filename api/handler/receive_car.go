package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/car_rental/go_api_gateway/genproto/order_service"
	"gitlab.com/car_rental/go_api_gateway/pkg/helper"
)

// CreateRecieveCar godoc
// @ID create_recieve_car
// @Router /recieve_car [POST]
// @Summary Create RecieveCar
// @Description Create RecieveCar
// @Tags RecieveCar
// @Accept json
// @Procedure json
// @Param recieve_car body order_service.RecieveCarCreate true "CreateRecieveCarRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) CreateRecieveCar(ctx *gin.Context) {
	var recieveCar order_service.RecieveCarCreate

	err := ctx.ShouldBind(&recieveCar)
	if err != nil {
		h.handlerResponse(ctx, "CreateRecieveCar", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.RecieveCar().Create(ctx, &recieveCar)
	if err != nil {
		h.handlerResponse(ctx, "RecieveCar().Create", http.StatusBadRequest, err.Error())

		return
	}

	h.handlerResponse(ctx, "create recieve_car response", http.StatusCreated, resp)
}

// GetByIdRecieveCar godoc
// @ID get_by_id_recieve_car
// @Router /recieve_car/{id} [GET]
// @Summary Get By ID RecieveCar
// @Description Get By ID RecieveCar
// @Tags RecieveCar
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) GetByIdRecieveCar(ctx *gin.Context) {
	recieveCarId := ctx.Param("id")

	if !helper.IsValidUUID(recieveCarId) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "Invalid uuid")
		return
	}

	resp, err := h.services.RecieveCar().GetById(ctx, &order_service.RecieveCarPrimaryKey{Id: recieveCarId})
	if err != nil {
		h.handlerResponse(ctx, "RecieveCar().GetById", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get by id recieve_car response", http.StatusOK, resp)
}

// RecieveCarGetList godoc
// @ID get_list_RecieveCar
// @Router /recieve_car [GET]
// @Summary Get List RecieveCar
// @Description Get List RecieveCar
// @Tags RecieveCar
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) RecieveCarGetList(ctx *gin.Context) {
	offset, err := h.getOffsetQuery(ctx.Query("offset"))
	if err != nil {
		h.handlerResponse(ctx, "get list recieve_car offset", http.StatusBadRequest, "invalid offset")
		return
	}

	limit, err := h.getLimitQuery(ctx.Query("limit"))
	if err != nil {
		h.handlerResponse(ctx, "get list recieve_car limit", http.StatusBadRequest, "invalid limit")
		return
	}

	resp, err := h.services.RecieveCar().GetList(ctx.Request.Context(), &order_service.RecieveCarGetListRequest{
		Offset: int64(offset),
		Limit:  int64(limit),
		Search: ctx.Query("search"),
	})
	if err != nil {
		h.handlerResponse(ctx, "RecieveCar().GetList", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "get list recieve_car response", http.StatusOK, resp)
}

// RecieveCarUpdate godoc
// @ID update_recieve_car
// @Router /recieve_car/{id} [PUT]
// @Summary Update RecieveCar
// @Description Update RecieveCar
// @Tags RecieveCar
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param recieve_car body order_service.RecieveCarUpdate true "UpdateRecieveCarRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) RecieveCarUpdate(ctx *gin.Context) {
	var (
		id               = ctx.Param("id")
		RecieveCarUpdate order_service.RecieveCarUpdate
	)

	if !helper.IsValidUUID(id) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	err := ctx.ShouldBindJSON(&RecieveCarUpdate)
	if err != nil {
		h.handlerResponse(ctx, "error recieve_car should bind json", http.StatusBadRequest, err.Error())
		return
	}

	RecieveCarUpdate.Id = id
	resp, err := h.services.RecieveCar().Update(ctx.Request.Context(), &RecieveCarUpdate)
	if err != nil {
		h.handlerResponse(ctx, "services.RecieveCar().Update", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "create recieve_car response", http.StatusAccepted, resp)
}

// DeleteRecieveCar godoc
// @ID delete_recieve_car
// @Router /recieve_car/{id} [DELETE]
// @Summary Delete RecieveCar
// @Description Delete RecieveCar
// @Tags RecieveCar
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) DeleteRecieveCar(c *gin.Context) {

	var id = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.handlerResponse(c, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	resp, err := h.services.RecieveCar().Delete(c.Request.Context(), &order_service.RecieveCarPrimaryKey{Id: id})
	if err != nil {
		h.handlerResponse(c, "services.RecieveCar().Delete", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "create recieve_car response", http.StatusNoContent, resp)
}
