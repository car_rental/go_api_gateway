package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/car_rental/go_api_gateway/genproto/car_service"
	"gitlab.com/car_rental/go_api_gateway/pkg/helper"
)

// Create car godoc
// @ID create_car
// @Router /car [POST]
// @Summary Create Car
// @Description Create Car
// @Tags Car
// @Accept json
// @Procedure json
// @Param car body car_service.CarCreate true "CreateCarRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) CreateCar(ctx *gin.Context) {
	var car car_service.CarCreate
	err := ctx.ShouldBind(&car)

	if err != nil {
		h.handlerResponse(ctx, "CreateCar", http.StatusBadRequest, err.Error())
		return
	}
	// fmt.Println(car)

	resp, err := h.services.CarService().Create(ctx, &car)
	if err != nil {
		h.handlerResponse(ctx, "CarService().Create", http.StatusBadRequest, err.Error())

		return
	}

	h.handlerResponse(ctx, "create car resposne", http.StatusCreated, resp)
}

// GetByID car godoc
// @ID get_by_id_car
// @Router /car/{id} [GET]
// @Summary Get By ID Car
// @Description Get By ID Car
// @Tags Car
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) GetByIdCar(ctx *gin.Context) {
	carId := ctx.Param("id")

	if !helper.IsValidUUID(carId) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "Invaid uuid")
		return
	}

	resp, err := h.services.CarService().GetById(ctx, &car_service.CarPrimaryKey{Id: carId})
	if err != nil {
		h.handlerResponse(ctx, "CarService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get by id car resposne", http.StatusOK, resp)
}

// GetList car godoc
// @ID get_list_Car
// @Router /car [GET]
// @Summary Get List Car
// @Description Get List Car
// @Tags Car
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param Search-By-Model query string false "search-by-model"
// @Param Search-By-Investor query string false "search-by-investor"
// @Param Search-By-Brand query string false "search-by-brand"
// @Param Search-By-StateNumber query string false "search-by-StateNumber"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) CarGetList(ctx *gin.Context) {
	offset, err := h.getOffsetQuery(ctx.Query("offset"))
	if err != nil {
		h.handlerResponse(ctx, "get list car offset", http.StatusBadRequest, "invalid offset")
		return
	}

	limit, err := h.getLimitQuery(ctx.Query("limit"))
	if err != nil {
		h.handlerResponse(ctx, "get list car limit", http.StatusBadRequest, "invalid limit")
		return
	}

	resp, err := h.services.CarService().GetList(ctx.Request.Context(), &car_service.CarGetListRequest{
		Offset:              int64(offset),
		Limit:               int64(limit),
		SearchByModel:       ctx.Query("search-by-model"),
		SearchByBrand:       ctx.Query("search-by-brand"),
		SearchByInvestorId:  ctx.Query("search-by-investor"),
		SearchByStateNumber: ctx.Query("search-by-StateNumber"),
	})
	if err != nil {
		h.handlerResponse(ctx, "CarService().GetList", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "get list car resposne", http.StatusOK, resp)
}

// Update car godoc
// @ID update_car
// @Router /car/{id} [PUT]
// @Summary Update Car
// @Description Update Car
// @Tags Car
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param car body car_service.CarUpdate true "UpdateCarRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) CarUpdate(ctx *gin.Context) {
	var (
		id        string = ctx.Param("id")
		Carupdate car_service.CarUpdate
	)

	if !helper.IsValidUUID(id) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	err := ctx.ShouldBindJSON(&Carupdate)
	if err != nil {
		h.handlerResponse(ctx, "error car should bind json", http.StatusBadRequest, err.Error())
		return
	}

	Carupdate.Id = id
	resp, err := h.services.CarService().Update(ctx.Request.Context(), &Carupdate)
	if err != nil {
		h.handlerResponse(ctx, "services.CarService().Update", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "create car resposne", http.StatusAccepted, resp)
}

// Delete car godoc
// @ID delete_car
// @Router /car/{id} [DELETE]
// @Summary Delete Car
// @Description Delete Car
// @Tags Car
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) DeleteCar(c *gin.Context) {

	var id string = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.handlerResponse(c, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	resp, err := h.services.CarService().Delete(c.Request.Context(), &car_service.CarPrimaryKey{Id: id})
	if err != nil {
		h.handlerResponse(c, "services.CarService().Delete", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "create car resposne", http.StatusNoContent, resp)
}
