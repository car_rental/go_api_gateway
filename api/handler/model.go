package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/car_rental/go_api_gateway/genproto/car_service"
	"gitlab.com/car_rental/go_api_gateway/pkg/helper"
)

// CreateModel godoc
// @ID create_model
// @Router /model [POST]
// @Summary Create Model
// @Description Create Model
// @Tags Model
// @Accept json
// @Procedure json
// @Param model body car_service.ModelCreate true "CreateModelRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) CreateModel(ctx *gin.Context) {
	var model car_service.ModelCreate

	err := ctx.ShouldBind(&model)
	if err != nil {
		h.handlerResponse(ctx, "CreateModel", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.ModelService().Create(ctx, &model)
	if err != nil {
		h.handlerResponse(ctx, "ModelService().Create", http.StatusBadRequest, err.Error())

		return
	}

	h.handlerResponse(ctx, "create model response", http.StatusCreated, resp)
}

// GetByIdModel godoc
// @ID get_by_id_model
// @Router /model/{id} [GET]
// @Summary Get By ID Model
// @Description Get By ID Model
// @Tags Model
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) GetByIdModel(ctx *gin.Context) {
	modelId := ctx.Param("id")

	if !helper.IsValidUUID(modelId) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "Invalid uuid")
		return
	}

	resp, err := h.services.ModelService().GetById(ctx, &car_service.ModelPrimaryKey{Id: modelId})
	if err != nil {
		h.handlerResponse(ctx, "ModelService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get by id model response", http.StatusOK, resp)
}

// ModelGetList godoc
// @ID get_list_Model
// @Router /model [GET]
// @Summary Get List Model
// @Description Get List Model
// @Tags Model
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) ModelGetList(ctx *gin.Context) {
	offset, err := h.getOffsetQuery(ctx.Query("offset"))
	if err != nil {
		h.handlerResponse(ctx, "get list model offset", http.StatusBadRequest, "invalid offset")
		return
	}

	limit, err := h.getLimitQuery(ctx.Query("limit"))
	if err != nil {
		h.handlerResponse(ctx, "get list model limit", http.StatusBadRequest, "invalid limit")
		return
	}

	resp, err := h.services.ModelService().GetList(ctx.Request.Context(), &car_service.ModelGetListRequest{
		Offset: int64(offset),
		Limit:  int64(limit),
		Search: ctx.Query("search"),
	})
	if err != nil {
		h.handlerResponse(ctx, "ModelService().GetList", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "get list model response", http.StatusOK, resp)
}

// ModelUpdate godoc
// @ID update_model
// @Router /model/{id} [PUT]
// @Summary Update Model
// @Description Update Model
// @Tags Model
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param model body car_service.ModelUpdate true "UpdateModelRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) ModelUpdate(ctx *gin.Context) {
	var (
		id          = ctx.Param("id")
		ModelUpdate car_service.ModelUpdate
	)

	if !helper.IsValidUUID(id) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	err := ctx.ShouldBindJSON(&ModelUpdate)
	if err != nil {
		h.handlerResponse(ctx, "error model should bind json", http.StatusBadRequest, err.Error())
		return
	}

	ModelUpdate.Id = id
	resp, err := h.services.ModelService().Update(ctx.Request.Context(), &ModelUpdate)
	if err != nil {
		h.handlerResponse(ctx, "services.ModelService().Update", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "create model response", http.StatusAccepted, resp)
}

// DeleteModel godoc
// @ID delete_model
// @Router /model/{id} [DELETE]
// @Summary Delete Model
// @Description Delete Model
// @Tags Model
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) DeleteModel(c *gin.Context) {
	var id = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.handlerResponse(c, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	resp, err := h.services.ModelService().Delete(c.Request.Context(), &car_service.ModelPrimaryKey{Id: id})
	if err != nil {
		h.handlerResponse(c, "services.ModelService().Delete", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "create model response", http.StatusNoContent, resp)
}
