package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/car_rental/go_api_gateway/genproto/order_service"
	"gitlab.com/car_rental/go_api_gateway/genproto/user_service"
	"gitlab.com/car_rental/go_api_gateway/pkg/helper"
)

// CreateGiveCar godoc
// @ID create_give_car
// @Router /give-car [POST]
// @Summary Create GiveCar
// @Description Create GiveCar
// @Tags GiveCar
// @Accept json
// @Procedure json
// @Param give-car body order_service.GiveCarCreate true "CreateGiveCarRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) CreateGiveCar(ctx *gin.Context) {
	var giveCar order_service.GiveCarCreate

	err := ctx.ShouldBind(&giveCar)
	if err != nil {
		h.handlerResponse(ctx, "CreateGiveCar", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.GiveCarService().Create(ctx, &giveCar)
	if err != nil {
		h.handlerResponse(ctx, "GiveCarService().Create", http.StatusBadRequest, err.Error())
		return
	}
	// sending email part
	order, err := h.services.OrderService().GetById(ctx, &order_service.OrderPrimaryKey{Id: resp.OrderId})
	if err != nil {
		h.handlerResponse(ctx, "GiveCarService().Create=> h.services.OrderService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	client, err := h.services.ClientService().GetById(ctx, &user_service.ClientPrimaryKey{Id: order.ClientId})
	if err != nil {
		h.handlerResponse(ctx, "GiveCarService().Create=> h.services.ClientService().GetById", http.StatusBadRequest, err.Error())
		return
	}
	err = h.SendMessageToMail("rentcartest123@gmail.com", "qngugcmjckgpuago", client.Email, "You have successfully taken your car")
	if err != nil {
		h.handlerResponse(ctx, "CreateGiveCar=>SendMessageToMail", http.StatusBadRequest, err.Error())
		return
	}
	h.handlerResponse(ctx, "create give car response", http.StatusCreated, resp)
}

// GetByIdGiveCar godoc
// @ID get_by_id_give_car
// @Router /give-car/{id} [GET]
// @Summary Get By ID GiveCar
// @Description Get By ID GiveCar
// @Tags GiveCar
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) GetByIdGiveCar(ctx *gin.Context) {
	giveCarId := ctx.Param("id")

	if !helper.IsValidUUID(giveCarId) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "Invalid uuid")
		return
	}

	resp, err := h.services.GiveCarService().GetById(ctx, &order_service.GiveCarPrimaryKey{Id: giveCarId})
	if err != nil {
		h.handlerResponse(ctx, "GiveCarService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get by id give-car response", http.StatusOK, resp)
}

// GiveCarGetList godoc
// @ID get_list_give_car
// @Router /give-car [GET]
// @Summary Get List GiveCar
// @Description Get List GiveCar
// @Tags GiveCar
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) GiveCarGetList(ctx *gin.Context) {
	offset, err := h.getOffsetQuery(ctx.Query("offset"))
	if err != nil {
		h.handlerResponse(ctx, "get list give-car offset", http.StatusBadRequest, "invalid offset")
		return
	}

	limit, err := h.getLimitQuery(ctx.Query("limit"))
	if err != nil {
		h.handlerResponse(ctx, "get list give-car limit", http.StatusBadRequest, "invalid limit")
		return
	}

	resp, err := h.services.GiveCarService().GetList(ctx.Request.Context(), &order_service.GiveCarGetListRequest{
		Offset: int64(offset),
		Limit:  int64(limit),
		Search: ctx.Query("search"),
	})
	if err != nil {
		h.handlerResponse(ctx, "GiveCarService().GetList", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "get list give-car response", http.StatusOK, resp)
}

// GiveCarUpdate godoc
// @ID update_give_car
// @Router /give-car/{id} [PUT]
// @Summary Update GiveCar
// @Description Update GiveCar
// @Tags GiveCar
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param give-car body order_service.GiveCarUpdate true "UpdateGiveCarRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) GiveCarUpdate(ctx *gin.Context) {
	var (
		id            = ctx.Param("id")
		GiveCarUpdate order_service.GiveCarUpdate
	)

	if !helper.IsValidUUID(id) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	err := ctx.ShouldBindJSON(&GiveCarUpdate)
	if err != nil {
		h.handlerResponse(ctx, "error give-car should bind json", http.StatusBadRequest, err.Error())
		return
	}

	GiveCarUpdate.Id = id
	resp, err := h.services.GiveCarService().Update(ctx.Request.Context(), &GiveCarUpdate)
	if err != nil {
		h.handlerResponse(ctx, "services.GiveCarService().Update", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "create GiveCar response", http.StatusAccepted, resp)
}

// DeleteGiveCar godoc
// @ID delete_give_car
// @Router /give-car/{id} [DELETE]
// @Summary Delete GiveCar
// @Description Delete GiveCar
// @Tags GiveCar
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) DeleteGiveCar(c *gin.Context) {

	var id = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.handlerResponse(c, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	resp, err := h.services.GiveCarService().Delete(c.Request.Context(), &order_service.GiveCarPrimaryKey{Id: id})
	if err != nil {
		h.handlerResponse(c, "services.GiveCarService().Delete", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "create give car response", http.StatusNoContent, resp)
}
