package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/car_rental/go_api_gateway/genproto/car_service"
	"gitlab.com/car_rental/go_api_gateway/pkg/helper"
)

// Create brand godoc
// @ID create_brand
// @Router /brand [POST]
// @Summary Create Brand
// @Description Create Brand
// @Tags Brand
// @Accept json
// @Procedure json
// @Param Brand body car_service.BrandCreate true "CreateBrandRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) CreateBrand(ctx *gin.Context) {
	var brand car_service.BrandCreate

	err := ctx.ShouldBind(&brand)
	if err != nil {
		h.handlerResponse(ctx, "CreateBrand", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.BrandService().Create(ctx, &brand)
	if err != nil {
		h.handlerResponse(ctx, "BrandService().Create", http.StatusBadRequest, err.Error())

		return
	}

	h.handlerResponse(ctx, "create Brand response", http.StatusCreated, resp)
}

// GetById brand godoc
// @ID get_by_id_brand
// @Router /brand/{id} [GET]
// @Summary Get By ID Brand
// @Description Get By ID Brand
// @Tags Brand
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) GetByIdBrand(ctx *gin.Context) {
	brandId := ctx.Param("id")

	if !helper.IsValidUUID(brandId) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "Invalid uuid")
		return
	}

	resp, err := h.services.BrandService().GetById(ctx, &car_service.BrandPrimaryKey{Id: brandId})
	if err != nil {
		h.handlerResponse(ctx, "BrandService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get by id Brand response", http.StatusOK, resp)
}

// GetList brand godoc
// @ID get_list_brand
// @Router /brand [GET]
// @Summary Get List Brand
// @Description Get List Brand
// @Tags Brand
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param Search query string false "search"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) BrandGetList(ctx *gin.Context) {
	offset, err := h.getOffsetQuery(ctx.Query("offset"))
	if err != nil {
		h.handlerResponse(ctx, "get list Brand offset", http.StatusBadRequest, "invalid offset")
		return
	}

	limit, err := h.getLimitQuery(ctx.Query("limit"))
	if err != nil {
		h.handlerResponse(ctx, "get list Brand limit", http.StatusBadRequest, "invalid limit")
		return
	}

	resp, err := h.services.BrandService().GetList(ctx.Request.Context(), &car_service.BrandGetListRequest{
		Offset: int64(offset),
		Limit:  int64(limit),
		Search: ctx.Query("search"),
	})
	if err != nil {
		h.handlerResponse(ctx, "BrandService().GetList", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "get list Brand response", http.StatusOK, resp)
}

// Update brand godoc
// @ID update_brand
// @Router /brand/{id} [PUT]
// @Summary Update Brand
// @Description Update Brand
// @Tags Brand
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param Brand body car_service.BrandUpdate true "UpdateBrandRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) BrandUpdate(ctx *gin.Context) {
	var (
		id          = ctx.Param("id")
		BrandUpdate car_service.BrandUpdate
	)

	if !helper.IsValidUUID(id) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	err := ctx.ShouldBindJSON(&BrandUpdate)
	if err != nil {
		h.handlerResponse(ctx, "error Brand should bind json", http.StatusBadRequest, err.Error())
		return
	}

	BrandUpdate.Id = id
	resp, err := h.services.BrandService().Update(ctx.Request.Context(), &BrandUpdate)
	if err != nil {
		h.handlerResponse(ctx, "services.BrandService().Update", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "create Brand response", http.StatusAccepted, resp)
}

// Delete brand godoc
// @ID delete_brand
// @Router /brand/{id} [DELETE]
// @Summary Delete Brand
// @Description Delete Brand
// @Tags Brand
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) DeleteBrand(c *gin.Context) {
	var id = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.handlerResponse(c, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	resp, err := h.services.BrandService().Delete(c.Request.Context(), &car_service.BrandPrimaryKey{Id: id})
	if err != nil {
		h.handlerResponse(c, "services.BrandService().Delete", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "create Brand response", http.StatusNoContent, resp)
}
