package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/car_rental/go_api_gateway/genproto/user_service"
	"gitlab.com/car_rental/go_api_gateway/pkg/helper"
)

// Create client godoc
// @ID create_client
// @Router /client [POST]
// @Summary Create Client
// @Description Create Client
// @Tags Client
// @Accept json
// @Procedure json
// @Param client body user_service.ClientCreate true "CreateClientRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) CreateClient(ctx *gin.Context) {
	var client user_service.ClientCreate

	err := ctx.ShouldBind(&client)
	if err != nil {
		h.handlerResponse(ctx, "CreateClient", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.ClientService().Create(ctx, &client)
	if err != nil {
		h.handlerResponse(ctx, "ClientService().Create", http.StatusBadRequest, err.Error())

		return
	}

	h.handlerResponse(ctx, "create client resposne", http.StatusCreated, resp)
}

// GetByID client godoc
// @ID get_by_id_client
// @Router /client/{id} [GET]
// @Summary Get By ID Client
// @Description Get By ID Client
// @Tags Client
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) GetByIdClient(ctx *gin.Context) {
	clientId := ctx.Param("id")

	if !helper.IsValidUUID(clientId) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "Invaid uuid")
		return
	}

	resp, err := h.services.ClientService().GetById(ctx, &user_service.ClientPrimaryKey{Id: clientId})
	if err != nil {
		h.handlerResponse(ctx, "ClientService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get by id client resposne", http.StatusOK, resp)
}

// GetList client godoc
// @ID get_list_client
// @Router /client [GET]
// @Summary Get List Client
// @Description Get List Client
// @Tags Client
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param Search query string false "search"
// @Param Search-By-Phone-Number query string false "search-by-phone-number"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) ClientGetList(ctx *gin.Context) {
	offset, err := h.getOffsetQuery(ctx.Query("offset"))
	if err != nil {
		h.handlerResponse(ctx, "get list client offset", http.StatusBadRequest, "invalid offset")
		return
	}

	limit, err := h.getLimitQuery(ctx.Query("limit"))
	if err != nil {
		h.handlerResponse(ctx, "get list client limit", http.StatusBadRequest, "invalid limit")
		return
	}

	resp, err := h.services.ClientService().GetList(ctx.Request.Context(), &user_service.ClientGetListRequest{
		Offset:              int64(offset),
		Limit:               int64(limit),
		Search:              ctx.Query("search"),
		SearchByPhoneNumber: ctx.Query("search-by-phone-number"),
	})
	if err != nil {
		h.handlerResponse(ctx, "ClientService().GetList", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "get list client resposne", http.StatusOK, resp)
}

// Update client godoc
// @ID update_client
// @Router /client/{id} [PUT]
// @Summary Update Client
// @Description Update Client
// @Tags Client
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param client body user_service.ClientUpdate true "UpdateClientRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) ClientUpdate(ctx *gin.Context) {
	var (
		id           string = ctx.Param("id")
		Clientupdate user_service.ClientUpdate
	)

	if !helper.IsValidUUID(id) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	err := ctx.ShouldBindJSON(&Clientupdate)
	if err != nil {
		h.handlerResponse(ctx, "error client should bind json", http.StatusBadRequest, err.Error())
		return
	}

	Clientupdate.Id = id
	resp, err := h.services.ClientService().Update(ctx.Request.Context(), &Clientupdate)
	if err != nil {
		h.handlerResponse(ctx, "services.ClientService().Update", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "create client resposne", http.StatusAccepted, resp)
}

// Delete client godoc
// @ID delete_client
// @Router /client/{id} [DELETE]
// @Summary Delete Client
// @Description Delete Client
// @Tags Client
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) DeleteClient(c *gin.Context) {

	var id string = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.handlerResponse(c, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	resp, err := h.services.ClientService().Delete(c.Request.Context(), &user_service.ClientPrimaryKey{Id: id})
	if err != nil {
		h.handlerResponse(c, "services.ClientService().Delete", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "create client resposne", http.StatusNoContent, resp)
}
