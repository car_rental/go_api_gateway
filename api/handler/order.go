package handler

import (
	"net/http"
	"time"

	"github.com/gin-gonic/gin"

	"gitlab.com/car_rental/go_api_gateway/genproto/car_service"
	"gitlab.com/car_rental/go_api_gateway/genproto/order_service"
	"gitlab.com/car_rental/go_api_gateway/genproto/user_service"
	"gitlab.com/car_rental/go_api_gateway/pkg/helper"
)

// CreateOrder godoc
// @ID create_order
// @Router /order [POST]
// @Summary Create Order
// @Description Create Order
// @Tags Order
// @Accept json
// @Procedure json
// @Param Order body order_service.OrderCreate true "CreateOrderRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) CreateOrder(ctx *gin.Context) {
	var (
		order order_service.OrderCreate
	)
	err := ctx.ShouldBind(&order)
	if err != nil {
		h.handlerResponse(ctx, "CreateOrder", http.StatusBadRequest, err.Error())
		return
	}

	// Check Car Status
	carStatus, err := h.services.CarService().GetById(ctx, &car_service.CarPrimaryKey{
		Id: order.CarId,
	})
	if err != nil {
		h.handlerResponse(ctx, "CreateOrder=>h.services.CarService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	if carStatus.Status != "in_stock" {
		h.handlerResponse(ctx, "This car used or booked", http.StatusBadRequest, "This car used or booked")
		return
	}

	// Update Car status To Booked
	respCar, err := h.services.CarService().Update(ctx, &car_service.CarUpdate{
		Id:          carStatus.Id,
		BrandId:     carStatus.BrandId,
		ModelId:     carStatus.ModelId,
		StateNumber: carStatus.StateNumber,
		Mileage:     carStatus.Mileage,
		InvestorId:  carStatus.InvestorId,
		Status:      "booked",
	})
	if err != nil {
		h.handlerResponse(ctx, "CreateOrder=>h.services.CarService().Update", http.StatusBadRequest, err.Error())
		return
	}

	_, err = h.services.CarActivityService().Create(ctx, &car_service.CarActivityCreate{
		StateNumber: carStatus.StateNumber,
		CarId:       carStatus.Id,
		Status:      respCar.Status,
		Date:        time.Now().Format("2006-01-02"),
	})

	if err != nil {
		h.handlerResponse(ctx, "CreateOrder=>h.services.CarActivityService().Create", http.StatusBadRequest, err.Error())
		return
	}

	// Get Client by id to send email
	clientEmail, err := h.services.ClientService().GetById(ctx, &user_service.ClientPrimaryKey{
		Id: order.ClientId,
	})
	if err != nil {
		h.handlerResponse(ctx, "CreateOrder=>h.services.ClientService().GetById", http.StatusBadRequest, err.Error())
	}

	// There We Send Message To the Client
	h.SendMessageToMail("rentcartest123@gmail.com", "qngugcmjckgpuago", clientEmail.Email, "Your Car Booked")

	resp, err := h.services.OrderService().Create(ctx, &order)
	if err != nil {
		h.handlerResponse(ctx, "OrderService().Create", http.StatusBadRequest, err.Error())

		return
	}

	h.handlerResponse(ctx, "create order response", http.StatusCreated, resp)
}

// GetByIdOrder godoc
// @ID get_by_id_order
// @Router /order/{id} [GET]
// @Summary Get By ID Order
// @Description Get By ID Order
// @Tags Order
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) GetByIdOrder(ctx *gin.Context) {
	orderId := ctx.Param("id")

	if !helper.IsValidUUID(orderId) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "Invalid uuid")
		return
	}

	resp, err := h.services.OrderService().GetById(ctx, &order_service.OrderPrimaryKey{Id: orderId})
	if err != nil {
		h.handlerResponse(ctx, "OrderService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get by id order response", http.StatusOK, resp)
}

// OrderGetList godoc
// @ID get_list_order
// @Router /order [GET]
// @Summary Get List Order
// @Description Get List Order
// @Tags Order
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param Search-By-Tarif query string false "search-by-tarif"
// @Param Search-By-Order-Id query string false "search-by-order-id"
// @Param Search-By-Date query string false "search-by-date"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) OrderGetList(ctx *gin.Context) {
	offset, err := h.getOffsetQuery(ctx.Query("offset"))
	if err != nil {
		h.handlerResponse(ctx, "get list order offset", http.StatusBadRequest, "invalid offset")
		return
	}

	limit, err := h.getLimitQuery(ctx.Query("limit"))
	if err != nil {
		h.handlerResponse(ctx, "get list order limit", http.StatusBadRequest, "invalid limit")
		return
	}

	resp, err := h.services.OrderService().GetList(ctx.Request.Context(), &order_service.OrderGetListRequest{
		Offset:                int64(offset),
		Limit:                 int64(limit),
		SearchByTarif:         ctx.Query("search-by-tarif"),
		SearchByOrderUniqueId: ctx.Query("search-by-order-id"),
		SearchByDate:          ctx.Query("search-by-date"),
	})
	if err != nil {
		h.handlerResponse(ctx, "OrderService().GetList", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "get list order response", http.StatusOK, resp)
}

// OrderUpdate godoc
// @ID update_order
// @Router /order/{id} [PUT]
// @Summary Update Order
// @Description Update Order
// @Tags Order
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param order body order_service.OrderUpdate true "UpdateOrderRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) OrderUpdate(ctx *gin.Context) {
	var (
		id          = ctx.Param("id")
		OrderUpdate order_service.OrderUpdate
	)

	if !helper.IsValidUUID(id) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	err := ctx.ShouldBindJSON(&OrderUpdate)
	if err != nil {
		h.handlerResponse(ctx, "error order should bind json", http.StatusBadRequest, err.Error())
		return
	}

	OrderUpdate.Id = id
	resp, err := h.services.OrderService().Update(ctx.Request.Context(), &OrderUpdate)
	if err != nil {
		h.handlerResponse(ctx, "services.OrderService().Update", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "create order response", http.StatusAccepted, resp)
}

// DeleteOrder godoc
// @ID delete_order
// @Router /order/{id} [DELETE]
// @Summary Delete Order
// @Description Delete Order
// @Tags Order
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) DeleteOrder(c *gin.Context) {

	var id = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.handlerResponse(c, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	resp, err := h.services.OrderService().Delete(c.Request.Context(), &order_service.OrderPrimaryKey{Id: id})
	if err != nil {
		h.handlerResponse(c, "services.OrderService().Delete", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "create order response", http.StatusNoContent, resp)
}
