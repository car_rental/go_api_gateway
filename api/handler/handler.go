package handler

import (
	"strconv"

	"github.com/gin-gonic/gin"
	"gopkg.in/gomail.v2"

	"gitlab.com/car_rental/go_api_gateway/config"
	"gitlab.com/car_rental/go_api_gateway/pkg/logger"
	"gitlab.com/car_rental/go_api_gateway/services"
)

type Handler struct {
	cfg      config.Config
	log      logger.LoggerI
	services services.ServiceManagerI
}

func NewHandler(cfg config.Config, log logger.LoggerI, srvc services.ServiceManagerI) *Handler {
	return &Handler{
		cfg:      cfg,
		log:      log,
		services: srvc,
	}
}

type Response struct {
	Status      int         `json:"status"`
	Description string      `json:"description"`
	Data        interface{} `json:"data"`
}

func (h *Handler) handlerResponse(c *gin.Context, path string, code int, message interface{}) {
	response := Response{
		Status: code,
		Data:   message,
	}

	switch {
	case code < 300:
		h.log.Info(path, logger.Any("info", response))
	case code >= 400:
		h.log.Error(path, logger.Any("error", response))
	}

	c.JSON(code, response)
}

func (h *Handler) getOffsetQuery(offset string) (int, error) {

	if len(offset) <= 0 {
		return 0, nil
	}

	return strconv.Atoi(offset)
}

func (h *Handler) getLimitQuery(limit string) (int, error) {

	if len(limit) <= 0 {
		return 10, nil
	}

	return strconv.Atoi(limit)
}

func (h *Handler) SendMessageToMail(sEmail string, sPassword string, To string, Message string) error {
	// Set up the email message
	subject := "Car Rental"
	messageBody := Message

	// Create a new email message using gomail
	m := gomail.NewMessage()
	m.SetHeader("From", sEmail)
	m.SetHeader("To", To)
	m.SetHeader("Subject", subject)
	m.SetBody("text/plain", messageBody)

	// Set up the email sending configuration
	d := gomail.NewDialer("smtp.gmail.com", 587, sEmail, sPassword)

	// Send the email
	if err := d.DialAndSend(m); err != nil {
		return err
	}
	return nil
}
