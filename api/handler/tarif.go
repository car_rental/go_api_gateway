package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/car_rental/go_api_gateway/genproto/order_service"
	"gitlab.com/car_rental/go_api_gateway/pkg/helper"
)

// Create tarif godoc
// @ID create_tarif
// @Router /tarif [POST]
// @Summary Create Tarif
// @Description Create Tarif
// @Tags Tarif
// @Accept json
// @Procedure json
// @Param Tarif body order_service.TarifCreate true "CreateTarifRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) CreateTarif(ctx *gin.Context) {
	var tarif order_service.TarifCreate

	err := ctx.ShouldBind(&tarif)
	if err != nil {
		h.handlerResponse(ctx, "CreateTarif", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.TarifService().Create(ctx, &tarif)
	if err != nil {
		h.handlerResponse(ctx, "TarifService().Create", http.StatusBadRequest, err.Error())

		return
	}

	h.handlerResponse(ctx, "create tarif resposne", http.StatusCreated, resp)
}

// GetByID tarif godoc
// @ID get_by_id_tarif
// @Router /tarif/{id} [GET]
// @Summary Get By ID Tarif
// @Description Get By ID Tarif
// @Tags Tarif
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) GetByIdTarif(ctx *gin.Context) {
	tarifId := ctx.Param("id")

	if !helper.IsValidUUID(tarifId) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "Invaid uuid")
		return
	}

	resp, err := h.services.TarifService().GetById(ctx, &order_service.TarifPrimaryKey{Id: tarifId})
	if err != nil {
		h.handlerResponse(ctx, "TarifService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	ctx.JSON(http.StatusOK, resp)
	h.handlerResponse(ctx, "get by id tarif resposne", http.StatusOK, resp)
}

// GetList tarif godoc
// @ID get_list_tarif
// @Router /tarif [GET]
// @Summary Get List Tarif
// @Description Get List Tarif
// @Tags Tarif
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) TarifGetList(ctx *gin.Context) {
	offset, err := h.getOffsetQuery(ctx.Query("offset"))
	if err != nil {
		h.handlerResponse(ctx, "get list tarif offset", http.StatusBadRequest, "invalid offset")
		return
	}

	limit, err := h.getLimitQuery(ctx.Query("limit"))
	if err != nil {
		h.handlerResponse(ctx, "get list tarif limit", http.StatusBadRequest, "invalid limit")
		return
	}

	resp, err := h.services.TarifService().GetList(ctx.Request.Context(), &order_service.TarifGetListRequest{
		Offset: int64(offset),
		Limit:  int64(limit),
		Search: ctx.Query("search"),
	})
	if err != nil {
		h.handlerResponse(ctx, "TarifService().GetList", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "get list tarif resposne", http.StatusOK, resp)
}

// Update tarif godoc
// @ID update_tarif
// @Router /tarif/{id} [PUT]
// @Summary Update Tarif
// @Description Update Tarif
// @Tags Tarif
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param tarif body order_service.TarifUpdate true "UpdateTarifRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) TarifUpdate(ctx *gin.Context) {
	var (
		id          string = ctx.Param("id")
		Tarifupdate order_service.TarifUpdate
	)

	if !helper.IsValidUUID(id) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	err := ctx.ShouldBindJSON(&Tarifupdate)
	if err != nil {
		h.handlerResponse(ctx, "error tarif should bind json", http.StatusBadRequest, err.Error())
		return
	}

	Tarifupdate.Id = id
	resp, err := h.services.TarifService().Update(ctx.Request.Context(), &Tarifupdate)
	if err != nil {
		h.handlerResponse(ctx, "services.TarifService().Update", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "create tarif resposne", http.StatusAccepted, resp)
}

// Delete tarif godoc
// @ID delete_tarif
// @Router /tarif/{id} [DELETE]
// @Summary Delete Tarif
// @Description Delete Tarif
// @Tags Tarif
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) DeleteTarif(c *gin.Context) {

	var id string = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.handlerResponse(c, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	resp, err := h.services.TarifService().Delete(c.Request.Context(), &order_service.TarifPrimaryKey{Id: id})
	if err != nil {
		h.handlerResponse(c, "services.TarifService().Delete", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "create tarif resposne", http.StatusNoContent, resp)
}
