package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/car_rental/go_api_gateway/genproto/user_service"
	"gitlab.com/car_rental/go_api_gateway/pkg/helper"
)

// Create mechanic godoc
// @ID create_mechanic
// @Router /mechanic [POST]
// @Summary Create Mechanic
// @Description Create Mechanic
// @Tags Mechanic
// @Accept json
// @Procedure json
// @Param mechanic body user_service.MechanicCreate true "CreateMechanicRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) CreateMechanic(ctx *gin.Context) {
	var mechanic user_service.MechanicCreate

	err := ctx.ShouldBind(&mechanic)
	if err != nil {
		h.handlerResponse(ctx, "CreateMechanic", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.MechanicService().Create(ctx, &mechanic)
	if err != nil {
		h.handlerResponse(ctx, "MechanicService().Create", http.StatusBadRequest, err.Error())

		return
	}

	h.handlerResponse(ctx, "create mechanic resposne", http.StatusCreated, resp)
}

// GetByID mechanic godoc
// @ID get_by_id_mechanic
// @Router /mechanic/{id} [GET]
// @Summary Get By ID Mechanic
// @Description Get By ID Mechanic
// @Tags Mechanic
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) GetByIdMechanic(ctx *gin.Context) {
	mechanicId := ctx.Param("id")

	if !helper.IsValidUUID(mechanicId) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "Invaid uuid")
		return
	}

	resp, err := h.services.MechanicService().GetById(ctx, &user_service.MechanicPrimaryKey{Id: mechanicId})
	if err != nil {
		h.handlerResponse(ctx, "MechanicService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get by id mechanic resposne", http.StatusOK, resp)
}

// GetList mechanic godoc
// @ID get_list_mechanic
// @Router /mechanic [GET]
// @Summary Get List Mechanic
// @Description Get List Mechanic
// @Tags Mechanic
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) MechanicGetList(ctx *gin.Context) {
	offset, err := h.getOffsetQuery(ctx.Query("offset"))
	if err != nil {
		h.handlerResponse(ctx, "get list mechanic offset", http.StatusBadRequest, "invalid offset")
		return
	}

	limit, err := h.getLimitQuery(ctx.Query("limit"))
	if err != nil {
		h.handlerResponse(ctx, "get list mechanic limit", http.StatusBadRequest, "invalid limit")
		return
	}

	resp, err := h.services.MechanicService().GetList(ctx.Request.Context(), &user_service.MechanicGetListRequest{
		Offset: int64(offset),
		Limit:  int64(limit),
		Search: ctx.Query("search"),
	})
	if err != nil {
		h.handlerResponse(ctx, "MechanicService().GetList", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "get list mechanic resposne", http.StatusOK, resp)
}

// Update mechanic godoc
// @ID update_mechanic
// @Router /mechanic/{id} [PUT]
// @Summary Update Mechanic
// @Description Update Mechanic
// @Tags Mechanic
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param mechanic body user_service.MechanicUpdate true "UpdateMechanicRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) MechanicUpdate(ctx *gin.Context) {
	var (
		id             string = ctx.Param("id")
		Mechanicupdate user_service.MechanicUpdate
	)

	if !helper.IsValidUUID(id) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	err := ctx.ShouldBindJSON(&Mechanicupdate)
	if err != nil {
		h.handlerResponse(ctx, "error mechanic should bind json", http.StatusBadRequest, err.Error())
		return
	}

	Mechanicupdate.Id = id
	resp, err := h.services.MechanicService().Update(ctx.Request.Context(), &Mechanicupdate)
	if err != nil {
		h.handlerResponse(ctx, "services.MechanicService().Update", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "create mechanic resposne", http.StatusAccepted, resp)
}

// Delete mechanic godoc
// @ID delete_mechanic
// @Router /mechanic/{id} [DELETE]
// @Summary Delete Mechanic
// @Description Delete Mechanic
// @Tags Mechanic
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) DeleteMechanic(c *gin.Context) {

	var id string = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.handlerResponse(c, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	resp, err := h.services.MechanicService().Delete(c.Request.Context(), &user_service.MechanicPrimaryKey{Id: id})
	if err != nil {
		h.handlerResponse(c, "services.MechanicService().Delete", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "create mechanic resposne", http.StatusNoContent, resp)
}
