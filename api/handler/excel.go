package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/car_rental/go_api_gateway/genproto/order_service"
)

// DownloadExcel godoc
// @ID DownloadExcel
// @Router /download-excel/{id} [GET]
// @Summary DownloadExcel
// @Description DownloadExcel
// @Tags Excel
// @Accept json
// @Procedure json
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) DownloadExcel(ctx *gin.Context) {

	resp, err := h.services.OrderService().Download(ctx, &order_service.ExcelRequest{})
	if err != nil {
		h.handlerResponse(ctx, "OrderService().Download", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "Download Excel", http.StatusOK, resp)
}
