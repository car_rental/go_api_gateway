package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/car_rental/go_api_gateway/genproto/car_service"
)

func (h *Handler) CreateCarActivity(ctx *gin.Context) {
	var car car_service.CarActivityCreate
	err := ctx.ShouldBind(&car)

	if err != nil {
		h.handlerResponse(ctx, "CreateCarActivity", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.CarActivityService().Create(ctx, &car)
	if err != nil {
		h.handlerResponse(ctx, "CreateCarActivity().Create", http.StatusBadRequest, err.Error())

		return
	}

	h.handlerResponse(ctx, "create carActivity resposne", http.StatusCreated, resp)
}

// GetList carActivity godoc
// @ID get_list_CarActivity
// @Router /car-activity [GET]
// @Summary Get List CarActivity
// @Description Get List CarActivity
// @Tags CarActivity
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param from query string false "from"
// @Param to query string false "to"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) CreateCarActivityGetList(ctx *gin.Context) {
	offset, err := h.getOffsetQuery(ctx.Query("offset"))
	if err != nil {
		h.handlerResponse(ctx, "get list CarActivity offset", http.StatusBadRequest, "invalid offset")
		return
	}

	limit, err := h.getLimitQuery(ctx.Query("limit"))
	if err != nil {
		h.handlerResponse(ctx, "get list CarActivity limit", http.StatusBadRequest, "invalid limit")
		return
	}

	resp, err := h.services.CarActivityService().GetList(ctx.Request.Context(), &car_service.CarActivityGetListRequest{
		Offset: int64(offset),
		Limit:  int64(limit),
		From:   ctx.Query("from"),
		To:     ctx.Query("to"),
	})
	if err != nil {
		h.handlerResponse(ctx, "CarActivityService().GetList", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "get list CarActivity resposne", http.StatusOK, resp)
}
