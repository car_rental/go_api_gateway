package api

import (
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"

	// "github.com/swaggo/swag/example/basic/docs"

	"gitlab.com/car_rental/go_api_gateway/api/docs"
	_ "gitlab.com/car_rental/go_api_gateway/api/docs"
	"gitlab.com/car_rental/go_api_gateway/api/handler"
	"gitlab.com/car_rental/go_api_gateway/config"
)

func SetUpApi(r *gin.Engine, h *handler.Handler, cfg config.Config) {
	docs.SwaggerInfo.Title = cfg.ServiceName
	docs.SwaggerInfo.Version = cfg.Version
	docs.SwaggerInfo.Schemes = []string{cfg.HTTPScheme}

	r.Use(customCORSMiddleware())
	r.Use(MaxAllowed(500))

	// User Services Api
	// Client Api
	r.POST("/client", h.CreateClient)
	r.GET("/client/:id", h.GetByIdClient)
	r.GET("client", h.ClientGetList)
	r.PUT("/client/:id", h.ClientUpdate)
	r.DELETE("/client/:id", h.DeleteClient)

	// Investor Api
	r.POST("/investor", h.CreateInvestor)
	r.GET("/investor/:id", h.GetByIdInvestor)
	r.GET("investor", h.InvestorGetList)
	r.PUT("/investor/:id", h.InvestorUpdate)
	r.DELETE("/investor/:id", h.DeleteInvestor)

	// Mechanic api
	r.POST("/mechanic", h.CreateMechanic)
	r.GET("/mechanic/:id", h.GetByIdMechanic)
	r.GET("mechanic", h.MechanicGetList)
	r.PUT("/mechanic/:id", h.MechanicUpdate)
	r.DELETE("/mechanic/:id", h.DeleteMechanic)

	// branch api
	r.POST("/branch", h.CreateBranch)
	r.GET("/branch/:id", h.GetByIdBranch)
	r.GET("branch", h.BranchGetList)
	r.PUT("/branch/:id", h.BranchUpdate)
	r.DELETE("/branch/:id", h.DeleteBranch)

	// Car Services Api
	// Car Api
	r.POST("/car", h.CreateCar)
	r.GET("/car/:id", h.GetByIdCar)
	r.GET("car", h.CarGetList)
	r.PUT("/car/:id", h.CarUpdate)
	r.DELETE("/car/:id", h.DeleteCar)
	// Model api
	r.POST("/model", h.CreateModel)
	r.GET("/model/:id", h.GetByIdModel)
	r.GET("model", h.ModelGetList)
	r.PUT("/model/:id", h.ModelUpdate)
	r.DELETE("/model/:id", h.DeleteModel)
	//brand api
	r.POST("/brand", h.CreateBrand)
	r.GET("/brand/:id", h.GetByIdBrand)
	r.GET("brand", h.BrandGetList)
	r.PUT("/brand/:id", h.BrandUpdate)
	r.DELETE("/brand/:id", h.DeleteBrand)

	r.GET("car-activity", h.CreateCarActivityGetList)

	// Order Services Api
	// Order Api
	r.POST("/order", h.CreateOrder)
	r.GET("/order/:id", h.GetByIdOrder)
	r.GET("order", h.OrderGetList)
	r.PUT("/order/:id", h.OrderUpdate)
	r.DELETE("/order/:id", h.DeleteOrder)

	// Order-Payment Api
	r.POST("/order_payment", h.CreateOrderPayment)
	r.GET("/order_payment/:id", h.GetByIdOrderPayment)
	r.GET("order_payment", h.OrderPaymentGetList)
	r.PUT("/order_payment/:id", h.OrderPaymentUpdate)
	r.DELETE("/order_payment/:id", h.DeleteOrderPayment)

	// Tarif Api
	r.POST("/tarif", h.CreateTarif)
	r.GET("/tarif/:id", h.GetByIdTarif)
	r.GET("tarif", h.TarifGetList)
	r.PUT("/tarif/:id", h.TarifUpdate)
	r.DELETE("/tarif/:id", h.DeleteTarif)

	// RecieveCar Api
	r.POST("/receive_car", h.CreateRecieveCar)
	r.GET("/receive_car/:id", h.GetByIdRecieveCar)
	r.GET("receive_car", h.RecieveCarGetList)
	r.PUT("/receive_car/:id", h.RecieveCarUpdate)
	r.DELETE("/receive_car/:id", h.DeleteRecieveCar)

	// GiveCar Api
	r.POST("/givecar", h.CreateGiveCar)
	r.GET("/givecar/:id", h.GetByIdGiveCar)
	r.GET("givecar", h.GiveCarGetList)
	r.PUT("/givecar/:id", h.GiveCarUpdate)
	r.DELETE("/givecar/:id", h.DeleteGiveCar)

	r.GET("/download-excel/:id", h.GetByIdGiveCar)

	url := ginSwagger.URL("swagger/doc.json")
	r.GET("swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))
}

func MaxAllowed(n int) gin.HandlerFunc {
	var countReq int64
	sem := make(chan struct{}, n)
	acquire := func() {
		sem <- struct{}{}
		countReq++
	}

	release := func() {
		select {
		case <-sem:
		default:
		}
		countReq--
	}

	return func(c *gin.Context) {
		acquire()       // before request
		defer release() // after request

		c.Set("sem", sem)
		c.Set("count_request", countReq)

		c.Next()
	}
}

func customCORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {

		c.Header("Access-Control-Allow-Origin", "*")
		c.Header("Access-Control-Allow-Credentials", "true")
		c.Header("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, PATCH, DELETE, HEAD")
		c.Header("Access-Control-Allow-Headers", "Platform-Id, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Header("Access-Control-Max-Age", "3600")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}
