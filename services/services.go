package services

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	"gitlab.com/car_rental/go_api_gateway/config"
	"gitlab.com/car_rental/go_api_gateway/genproto/car_service"
	"gitlab.com/car_rental/go_api_gateway/genproto/order_service"
	"gitlab.com/car_rental/go_api_gateway/genproto/user_service"
)

type ServiceManagerI interface {
	// User Service
	ClientService() user_service.ClientServiceClient
	InvestorService() user_service.InvestorServiceClient
	MechanicService() user_service.MechanicServiceClient
	BranchService() user_service.BranchServiceClient

	// Car Service
	CarService() car_service.CarServiceClient
	ModelService() car_service.ModelServiceClient
	BrandService() car_service.BrandServiceClient

	CarActivityService() car_service.CarActivityServiceClient

	// Order Service
	OrderService() order_service.OrderServiceClient
	TarifService() order_service.TarifServiceClient
	RecieveCar() order_service.RecieveCarServiceClient
	GiveCarService() order_service.GiveCarServiceClient
	OrderPaymentService() order_service.OrderPaymentServiceClient
}

type grpcClients struct {
	// User Service
	clientService   user_service.ClientServiceClient
	investorService user_service.InvestorServiceClient
	mechanicService user_service.MechanicServiceClient
	branchService   user_service.BranchServiceClient

	// Car Service
	carService         car_service.CarServiceClient
	modelService       car_service.ModelServiceClient
	brandService       car_service.BrandServiceClient
	caractivityService car_service.CarActivityServiceClient

	// Order Service
	orderService        order_service.OrderServiceClient
	tarifService        order_service.TarifServiceClient
	recieveCarService   order_service.RecieveCarServiceClient
	giveCarService      order_service.GiveCarServiceClient
	orderPaymentService order_service.OrderPaymentServiceClient
}

func NewGrpcClients(cfg config.Config) (ServiceManagerI, error) {
	// User Microservice
	connUserService, err := grpc.Dial(
		cfg.UserServiceHost+cfg.UserGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)

	if err != nil {
		return nil, err
	}

	// Car Microservice
	connCarService, err := grpc.Dial(
		cfg.CarServiceHost+cfg.CarGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)

	if err != nil {
		return nil, err
	}

	// Order Microservice
	connOrderService, err := grpc.Dial(
		cfg.OrderServiceHost+cfg.OrderGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)

	if err != nil {
		return nil, err
	}

	return &grpcClients{
		// User Service
		clientService:   user_service.NewClientServiceClient(connUserService),
		investorService: user_service.NewInvestorServiceClient(connUserService),
		mechanicService: user_service.NewMechanicServiceClient(connUserService),
		branchService:   user_service.NewBranchServiceClient(connUserService),

		// Car Service
		carService:         car_service.NewCarServiceClient(connCarService),
		modelService:       car_service.NewModelServiceClient(connCarService),
		brandService:       car_service.NewBrandServiceClient(connCarService),
		caractivityService: car_service.NewCarActivityServiceClient(connCarService),

		// Order Service
		orderService:        order_service.NewOrderServiceClient(connOrderService),
		tarifService:        order_service.NewTarifServiceClient(connOrderService),
		recieveCarService:   order_service.NewRecieveCarServiceClient(connOrderService),
		giveCarService:      order_service.NewGiveCarServiceClient(connOrderService),
		orderPaymentService: order_service.NewOrderPaymentServiceClient(connOrderService),
	}, nil
}

// User Service

func (g *grpcClients) ClientService() user_service.ClientServiceClient {
	return g.clientService
}

func (g *grpcClients) InvestorService() user_service.InvestorServiceClient {
	return g.investorService
}

func (g *grpcClients) MechanicService() user_service.MechanicServiceClient {
	return g.mechanicService
}
func (g *grpcClients) BranchService() user_service.BranchServiceClient {
	return g.branchService
}

// Car Service

func (g *grpcClients) CarService() car_service.CarServiceClient {
	return g.carService
}

func (g *grpcClients) ModelService() car_service.ModelServiceClient {
	return g.modelService
}
func (g *grpcClients) BrandService() car_service.BrandServiceClient {
	return g.brandService
}

func (g *grpcClients) CarActivityService() car_service.CarActivityServiceClient {
	return g.caractivityService
}

// Order Service

func (g *grpcClients) OrderService() order_service.OrderServiceClient {
	return g.orderService
}

func (g *grpcClients) TarifService() order_service.TarifServiceClient {
	return g.tarifService
}

func (g *grpcClients) RecieveCar() order_service.RecieveCarServiceClient {
	return g.recieveCarService
}

func (g *grpcClients) GiveCarService() order_service.GiveCarServiceClient {
	return g.giveCarService
}

func (g *grpcClients) OrderPaymentService() order_service.OrderPaymentServiceClient {
	return g.orderPaymentService
}
