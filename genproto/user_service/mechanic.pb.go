// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.28.1
// 	protoc        v3.12.4
// source: mechanic.proto

package user_service

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type Mechanic struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id          string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	Name        string `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	Birthday    string `protobuf:"bytes,3,opt,name=birthday,proto3" json:"birthday,omitempty"`
	PhoneNumber string `protobuf:"bytes,4,opt,name=phone_number,json=phoneNumber,proto3" json:"phone_number,omitempty"`
	CreatedAt   string `protobuf:"bytes,5,opt,name=created_at,json=createdAt,proto3" json:"created_at,omitempty"`
	UpdatedAt   string `protobuf:"bytes,6,opt,name=updated_at,json=updatedAt,proto3" json:"updated_at,omitempty"`
}

func (x *Mechanic) Reset() {
	*x = Mechanic{}
	if protoimpl.UnsafeEnabled {
		mi := &file_mechanic_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Mechanic) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Mechanic) ProtoMessage() {}

func (x *Mechanic) ProtoReflect() protoreflect.Message {
	mi := &file_mechanic_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Mechanic.ProtoReflect.Descriptor instead.
func (*Mechanic) Descriptor() ([]byte, []int) {
	return file_mechanic_proto_rawDescGZIP(), []int{0}
}

func (x *Mechanic) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *Mechanic) GetName() string {
	if x != nil {
		return x.Name
	}
	return ""
}

func (x *Mechanic) GetBirthday() string {
	if x != nil {
		return x.Birthday
	}
	return ""
}

func (x *Mechanic) GetPhoneNumber() string {
	if x != nil {
		return x.PhoneNumber
	}
	return ""
}

func (x *Mechanic) GetCreatedAt() string {
	if x != nil {
		return x.CreatedAt
	}
	return ""
}

func (x *Mechanic) GetUpdatedAt() string {
	if x != nil {
		return x.UpdatedAt
	}
	return ""
}

type MechanicPrimaryKey struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
}

func (x *MechanicPrimaryKey) Reset() {
	*x = MechanicPrimaryKey{}
	if protoimpl.UnsafeEnabled {
		mi := &file_mechanic_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *MechanicPrimaryKey) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*MechanicPrimaryKey) ProtoMessage() {}

func (x *MechanicPrimaryKey) ProtoReflect() protoreflect.Message {
	mi := &file_mechanic_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use MechanicPrimaryKey.ProtoReflect.Descriptor instead.
func (*MechanicPrimaryKey) Descriptor() ([]byte, []int) {
	return file_mechanic_proto_rawDescGZIP(), []int{1}
}

func (x *MechanicPrimaryKey) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

type MechanicCreate struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Name        string `protobuf:"bytes,1,opt,name=name,proto3" json:"name,omitempty"`
	Birthday    string `protobuf:"bytes,2,opt,name=birthday,proto3" json:"birthday,omitempty"`
	PhoneNumber string `protobuf:"bytes,3,opt,name=phone_number,json=phoneNumber,proto3" json:"phone_number,omitempty"`
}

func (x *MechanicCreate) Reset() {
	*x = MechanicCreate{}
	if protoimpl.UnsafeEnabled {
		mi := &file_mechanic_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *MechanicCreate) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*MechanicCreate) ProtoMessage() {}

func (x *MechanicCreate) ProtoReflect() protoreflect.Message {
	mi := &file_mechanic_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use MechanicCreate.ProtoReflect.Descriptor instead.
func (*MechanicCreate) Descriptor() ([]byte, []int) {
	return file_mechanic_proto_rawDescGZIP(), []int{2}
}

func (x *MechanicCreate) GetName() string {
	if x != nil {
		return x.Name
	}
	return ""
}

func (x *MechanicCreate) GetBirthday() string {
	if x != nil {
		return x.Birthday
	}
	return ""
}

func (x *MechanicCreate) GetPhoneNumber() string {
	if x != nil {
		return x.PhoneNumber
	}
	return ""
}

type MechanicUpdate struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id          string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	Name        string `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	Birthday    string `protobuf:"bytes,3,opt,name=birthday,proto3" json:"birthday,omitempty"`
	PhoneNumber string `protobuf:"bytes,4,opt,name=phone_number,json=phoneNumber,proto3" json:"phone_number,omitempty"`
}

func (x *MechanicUpdate) Reset() {
	*x = MechanicUpdate{}
	if protoimpl.UnsafeEnabled {
		mi := &file_mechanic_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *MechanicUpdate) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*MechanicUpdate) ProtoMessage() {}

func (x *MechanicUpdate) ProtoReflect() protoreflect.Message {
	mi := &file_mechanic_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use MechanicUpdate.ProtoReflect.Descriptor instead.
func (*MechanicUpdate) Descriptor() ([]byte, []int) {
	return file_mechanic_proto_rawDescGZIP(), []int{3}
}

func (x *MechanicUpdate) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *MechanicUpdate) GetName() string {
	if x != nil {
		return x.Name
	}
	return ""
}

func (x *MechanicUpdate) GetBirthday() string {
	if x != nil {
		return x.Birthday
	}
	return ""
}

func (x *MechanicUpdate) GetPhoneNumber() string {
	if x != nil {
		return x.PhoneNumber
	}
	return ""
}

type MechanicGetListRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Offset int64  `protobuf:"varint,1,opt,name=offset,proto3" json:"offset,omitempty"`
	Limit  int64  `protobuf:"varint,2,opt,name=limit,proto3" json:"limit,omitempty"`
	Search string `protobuf:"bytes,3,opt,name=search,proto3" json:"search,omitempty"`
}

func (x *MechanicGetListRequest) Reset() {
	*x = MechanicGetListRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_mechanic_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *MechanicGetListRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*MechanicGetListRequest) ProtoMessage() {}

func (x *MechanicGetListRequest) ProtoReflect() protoreflect.Message {
	mi := &file_mechanic_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use MechanicGetListRequest.ProtoReflect.Descriptor instead.
func (*MechanicGetListRequest) Descriptor() ([]byte, []int) {
	return file_mechanic_proto_rawDescGZIP(), []int{4}
}

func (x *MechanicGetListRequest) GetOffset() int64 {
	if x != nil {
		return x.Offset
	}
	return 0
}

func (x *MechanicGetListRequest) GetLimit() int64 {
	if x != nil {
		return x.Limit
	}
	return 0
}

func (x *MechanicGetListRequest) GetSearch() string {
	if x != nil {
		return x.Search
	}
	return ""
}

type MechanicGetListResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Count     int64       `protobuf:"varint,1,opt,name=count,proto3" json:"count,omitempty"`
	Mechanics []*Mechanic `protobuf:"bytes,2,rep,name=Mechanics,proto3" json:"Mechanics,omitempty"`
}

func (x *MechanicGetListResponse) Reset() {
	*x = MechanicGetListResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_mechanic_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *MechanicGetListResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*MechanicGetListResponse) ProtoMessage() {}

func (x *MechanicGetListResponse) ProtoReflect() protoreflect.Message {
	mi := &file_mechanic_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use MechanicGetListResponse.ProtoReflect.Descriptor instead.
func (*MechanicGetListResponse) Descriptor() ([]byte, []int) {
	return file_mechanic_proto_rawDescGZIP(), []int{5}
}

func (x *MechanicGetListResponse) GetCount() int64 {
	if x != nil {
		return x.Count
	}
	return 0
}

func (x *MechanicGetListResponse) GetMechanics() []*Mechanic {
	if x != nil {
		return x.Mechanics
	}
	return nil
}

var File_mechanic_proto protoreflect.FileDescriptor

var file_mechanic_proto_rawDesc = []byte{
	0x0a, 0x0e, 0x6d, 0x65, 0x63, 0x68, 0x61, 0x6e, 0x69, 0x63, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f,
	0x12, 0x0c, 0x75, 0x73, 0x65, 0x72, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x22, 0xab,
	0x01, 0x0a, 0x08, 0x4d, 0x65, 0x63, 0x68, 0x61, 0x6e, 0x69, 0x63, 0x12, 0x0e, 0x0a, 0x02, 0x69,
	0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x12, 0x12, 0x0a, 0x04, 0x6e,
	0x61, 0x6d, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x12,
	0x1a, 0x0a, 0x08, 0x62, 0x69, 0x72, 0x74, 0x68, 0x64, 0x61, 0x79, 0x18, 0x03, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x08, 0x62, 0x69, 0x72, 0x74, 0x68, 0x64, 0x61, 0x79, 0x12, 0x21, 0x0a, 0x0c, 0x70,
	0x68, 0x6f, 0x6e, 0x65, 0x5f, 0x6e, 0x75, 0x6d, 0x62, 0x65, 0x72, 0x18, 0x04, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x0b, 0x70, 0x68, 0x6f, 0x6e, 0x65, 0x4e, 0x75, 0x6d, 0x62, 0x65, 0x72, 0x12, 0x1d,
	0x0a, 0x0a, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x5f, 0x61, 0x74, 0x18, 0x05, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x09, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x41, 0x74, 0x12, 0x1d, 0x0a,
	0x0a, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x64, 0x5f, 0x61, 0x74, 0x18, 0x06, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x09, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x64, 0x41, 0x74, 0x22, 0x24, 0x0a, 0x12,
	0x4d, 0x65, 0x63, 0x68, 0x61, 0x6e, 0x69, 0x63, 0x50, 0x72, 0x69, 0x6d, 0x61, 0x72, 0x79, 0x4b,
	0x65, 0x79, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02,
	0x69, 0x64, 0x22, 0x63, 0x0a, 0x0e, 0x4d, 0x65, 0x63, 0x68, 0x61, 0x6e, 0x69, 0x63, 0x43, 0x72,
	0x65, 0x61, 0x74, 0x65, 0x12, 0x12, 0x0a, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x01, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x12, 0x1a, 0x0a, 0x08, 0x62, 0x69, 0x72, 0x74,
	0x68, 0x64, 0x61, 0x79, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x62, 0x69, 0x72, 0x74,
	0x68, 0x64, 0x61, 0x79, 0x12, 0x21, 0x0a, 0x0c, 0x70, 0x68, 0x6f, 0x6e, 0x65, 0x5f, 0x6e, 0x75,
	0x6d, 0x62, 0x65, 0x72, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0b, 0x70, 0x68, 0x6f, 0x6e,
	0x65, 0x4e, 0x75, 0x6d, 0x62, 0x65, 0x72, 0x22, 0x73, 0x0a, 0x0e, 0x4d, 0x65, 0x63, 0x68, 0x61,
	0x6e, 0x69, 0x63, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18,
	0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x12, 0x12, 0x0a, 0x04, 0x6e, 0x61, 0x6d,
	0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x12, 0x1a, 0x0a,
	0x08, 0x62, 0x69, 0x72, 0x74, 0x68, 0x64, 0x61, 0x79, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x08, 0x62, 0x69, 0x72, 0x74, 0x68, 0x64, 0x61, 0x79, 0x12, 0x21, 0x0a, 0x0c, 0x70, 0x68, 0x6f,
	0x6e, 0x65, 0x5f, 0x6e, 0x75, 0x6d, 0x62, 0x65, 0x72, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x0b, 0x70, 0x68, 0x6f, 0x6e, 0x65, 0x4e, 0x75, 0x6d, 0x62, 0x65, 0x72, 0x22, 0x5e, 0x0a, 0x16,
	0x4d, 0x65, 0x63, 0x68, 0x61, 0x6e, 0x69, 0x63, 0x47, 0x65, 0x74, 0x4c, 0x69, 0x73, 0x74, 0x52,
	0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x16, 0x0a, 0x06, 0x6f, 0x66, 0x66, 0x73, 0x65, 0x74,
	0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x06, 0x6f, 0x66, 0x66, 0x73, 0x65, 0x74, 0x12, 0x14,
	0x0a, 0x05, 0x6c, 0x69, 0x6d, 0x69, 0x74, 0x18, 0x02, 0x20, 0x01, 0x28, 0x03, 0x52, 0x05, 0x6c,
	0x69, 0x6d, 0x69, 0x74, 0x12, 0x16, 0x0a, 0x06, 0x73, 0x65, 0x61, 0x72, 0x63, 0x68, 0x18, 0x03,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x73, 0x65, 0x61, 0x72, 0x63, 0x68, 0x22, 0x65, 0x0a, 0x17,
	0x4d, 0x65, 0x63, 0x68, 0x61, 0x6e, 0x69, 0x63, 0x47, 0x65, 0x74, 0x4c, 0x69, 0x73, 0x74, 0x52,
	0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x14, 0x0a, 0x05, 0x63, 0x6f, 0x75, 0x6e, 0x74,
	0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x05, 0x63, 0x6f, 0x75, 0x6e, 0x74, 0x12, 0x34, 0x0a,
	0x09, 0x4d, 0x65, 0x63, 0x68, 0x61, 0x6e, 0x69, 0x63, 0x73, 0x18, 0x02, 0x20, 0x03, 0x28, 0x0b,
	0x32, 0x16, 0x2e, 0x75, 0x73, 0x65, 0x72, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e,
	0x4d, 0x65, 0x63, 0x68, 0x61, 0x6e, 0x69, 0x63, 0x52, 0x09, 0x4d, 0x65, 0x63, 0x68, 0x61, 0x6e,
	0x69, 0x63, 0x73, 0x42, 0x17, 0x5a, 0x15, 0x67, 0x65, 0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f,
	0x75, 0x73, 0x65, 0x72, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x62, 0x06, 0x70, 0x72,
	0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_mechanic_proto_rawDescOnce sync.Once
	file_mechanic_proto_rawDescData = file_mechanic_proto_rawDesc
)

func file_mechanic_proto_rawDescGZIP() []byte {
	file_mechanic_proto_rawDescOnce.Do(func() {
		file_mechanic_proto_rawDescData = protoimpl.X.CompressGZIP(file_mechanic_proto_rawDescData)
	})
	return file_mechanic_proto_rawDescData
}

var file_mechanic_proto_msgTypes = make([]protoimpl.MessageInfo, 6)
var file_mechanic_proto_goTypes = []interface{}{
	(*Mechanic)(nil),                // 0: user_service.Mechanic
	(*MechanicPrimaryKey)(nil),      // 1: user_service.MechanicPrimaryKey
	(*MechanicCreate)(nil),          // 2: user_service.MechanicCreate
	(*MechanicUpdate)(nil),          // 3: user_service.MechanicUpdate
	(*MechanicGetListRequest)(nil),  // 4: user_service.MechanicGetListRequest
	(*MechanicGetListResponse)(nil), // 5: user_service.MechanicGetListResponse
}
var file_mechanic_proto_depIdxs = []int32{
	0, // 0: user_service.MechanicGetListResponse.Mechanics:type_name -> user_service.Mechanic
	1, // [1:1] is the sub-list for method output_type
	1, // [1:1] is the sub-list for method input_type
	1, // [1:1] is the sub-list for extension type_name
	1, // [1:1] is the sub-list for extension extendee
	0, // [0:1] is the sub-list for field type_name
}

func init() { file_mechanic_proto_init() }
func file_mechanic_proto_init() {
	if File_mechanic_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_mechanic_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Mechanic); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_mechanic_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*MechanicPrimaryKey); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_mechanic_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*MechanicCreate); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_mechanic_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*MechanicUpdate); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_mechanic_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*MechanicGetListRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_mechanic_proto_msgTypes[5].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*MechanicGetListResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_mechanic_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   6,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_mechanic_proto_goTypes,
		DependencyIndexes: file_mechanic_proto_depIdxs,
		MessageInfos:      file_mechanic_proto_msgTypes,
	}.Build()
	File_mechanic_proto = out.File
	file_mechanic_proto_rawDesc = nil
	file_mechanic_proto_goTypes = nil
	file_mechanic_proto_depIdxs = nil
}
